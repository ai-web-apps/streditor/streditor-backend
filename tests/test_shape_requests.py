import cv2
import json
import pytest
import numpy as np

from streditor.utils.conversion import base64_to_numpy, numpy_to_base64


def test_resize(client, content_image):
    base64_image = numpy_to_base64(content_image)
    executions = json.dumps(
        {
            0: {
                'stage': 'size',
                'width': 125,
                'height': 368,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_image, _ = base64_to_numpy(returned_base_64_image)
    assert returned_image.shape == (368, 125, 3)


def test_resize_with_mask(client, style_image, style_mask):
    base64_image = numpy_to_base64(style_image)
    base64_mask = numpy_to_base64(style_mask)
    executions = json.dumps(
        {
            0: {
                'stage': 'size',
                'width': 190,
                'height': 112,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, mask=base64_mask, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_base_64_mask = completions['0']['mask']
    returned_image, _ = base64_to_numpy(returned_base_64_image, cv2.IMREAD_COLOR)
    returned_mask, _ = base64_to_numpy(returned_base_64_mask, cv2.IMREAD_GRAYSCALE)

    assert returned_image.shape == (112, 190, 3)
    assert returned_mask.shape == (112, 190)
    assert set(np.unique(style_mask)) == set(np.unique(returned_mask))


def test_crop(client, style_image):
    base64_image = numpy_to_base64(style_image)

    # expect smaller image
    executions = json.dumps(
        {
            0: {
                'stage': 'crop',
                'x': 0.1,
                'y': 0.1,
                'width': 0.65,
                'height': 0.4,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_image, _ = base64_to_numpy(returned_base_64_image)
    assert (style_image[22:112, 22:168] == returned_image).all()

    # expect assertion error for incorrect bounds
    executions = json.dumps(
        {
            0: {
                'stage': 'crop',
                'x': 0.3,
                'y': 0.3,
                'width': -0.25,
                'height': 0.15,
            }
        }
    )
    with pytest.raises(AssertionError):
        rv = client.post(
            '/preprocessing/execute/',
            data=dict(image=base64_image, executions=executions),
        )


def test_crop_with_mask(client, content_image, content_mask):
    base64_image = numpy_to_base64(content_image)
    base64_mask = numpy_to_base64(content_mask)

    # expect smaller image
    executions = json.dumps(
        {
            0: {
                'stage': 'crop',
                'x': 0.25,
                'y': 0.0,
                'width': 0.5,
                'height': 0.72,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, mask=base64_mask, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_base_64_mask = completions['0']['mask']
    returned_image, _ = base64_to_numpy(returned_base_64_image, cv2.IMREAD_COLOR)
    returned_mask, _ = base64_to_numpy(returned_base_64_mask, cv2.IMREAD_GRAYSCALE)

    assert (content_image[0:162, 75:225] == returned_image).all()
    assert (content_mask[0:162, 75:225] == returned_mask).all()

    # expect assertion error for incorrect bounds
    executions = json.dumps(
        {
            0: {
                'stage': 'crop',
                'x': 0.25,
                'y': 0.0,
                'width': 0.9,
                'height': 0.72,
            }
        }
    )
    with pytest.raises(AssertionError):
        rv = client.post(
            '/preprocessing/execute/',
            data=dict(image=base64_image, mask=base64_mask, executions=executions),
        )
