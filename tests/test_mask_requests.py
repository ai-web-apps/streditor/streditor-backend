import cv2
import numpy as np

from streditor.utils.conversion import base64_to_numpy, numpy_to_base64


def test_colour_mask(client, content_mask):
    base64_mask = numpy_to_base64(content_mask)

    rv = client.post('/masking/colour/', data=dict(mask=base64_mask))
    base64_colour_mask = rv.get_json()['colour_mask']
    numpy_colour_mask, _ = base64_to_numpy(base64_colour_mask, cv2.IMREAD_COLOR)

    assert numpy_colour_mask.shape == content_mask.shape + (3,)


def test_validate_masks(client, content_mask, style_mask):
    # success - no masks
    rv = client.post('/masking/validate/', data=dict())
    assert rv.get_json()['valid']

    # success - one single labelled mask
    new_content_mask = np.copy(content_mask)
    new_content_mask[content_mask > 0] = 20
    base64_content_mask = numpy_to_base64(new_content_mask)

    rv = client.post('/masking/validate/', data=dict(baseMask=base64_content_mask))
    assert rv.get_json()['valid']

    # success - two double labelled masks
    base64_content_mask = numpy_to_base64(content_mask)
    base64_style_mask = numpy_to_base64(style_mask)

    rv = client.post(
        '/masking/validate/',
        data=dict(baseMask=base64_content_mask, styleMask=base64_style_mask),
    )
    assert rv.get_json()['valid']

    # success - two single labelled masks with different labels
    new_content_mask = np.copy(content_mask)
    new_content_mask[content_mask > 0] = 10
    new_style_mask = np.copy(style_mask)
    new_style_mask[style_mask > 0] = 11
    base64_content_mask = numpy_to_base64(new_content_mask)
    base64_style_mask = numpy_to_base64(new_style_mask)

    rv = client.post(
        '/masking/validate/',
        data=dict(baseMask=base64_content_mask, styleMask=base64_style_mask),
    )
    assert rv.get_json()['valid']

    # failure - mismatching labels
    new_style_mask = np.copy(style_mask)
    new_style_mask[style_mask == 0] = 3
    base64_content_mask = numpy_to_base64(content_mask)
    base64_style_mask = numpy_to_base64(new_style_mask)

    rv = client.post(
        '/masking/validate/',
        data=dict(baseMask=base64_content_mask, styleMask=base64_style_mask),
    )
    assert not rv.get_json()['valid']
