import os
import cv2
import pytest
import tempfile

from streditor import create_app
from streditor.db import init_db


@pytest.fixture(scope='session')
def app():
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({'TESTING': True, 'DATABASE': db_path})

    with app.app_context():
        init_db()
    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture(scope='session')
def client(app):
    yield app.test_client()


@pytest.fixture(scope='session')
def cli_runner(app):
    yield app.test_cli_runner()


@pytest.fixture(scope='session')
def content_image():
    content_image_path = os.path.join('tests', 'data', 'content_image.jpg')
    yield cv2.imread(content_image_path, cv2.IMREAD_COLOR)


@pytest.fixture(scope='session')
def style_image():
    style_image_path = os.path.join('tests', 'data', 'style_image.jpg')
    yield cv2.imread(style_image_path, cv2.IMREAD_COLOR)


@pytest.fixture(scope='session')
def content_mask():
    content_mask_path = os.path.join('tests', 'data', 'content_mask.png')
    yield cv2.imread(content_mask_path, cv2.IMREAD_GRAYSCALE)


@pytest.fixture(scope='session')
def style_mask():
    style_mask_path = os.path.join('tests', 'data', 'style_mask.png')
    yield cv2.imread(style_mask_path, cv2.IMREAD_GRAYSCALE)
