import cv2
import json

from streditor.utils.conversion import base64_to_numpy, numpy_to_base64


def test_unchanged_with_image_only(client, content_image):
    base64_image = numpy_to_base64(content_image)

    # expect unchanged
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 50,
                'contrast': 50,
                'red': 50,
                'green': 50,
                'blue': 50,
            },
            1: {'stage': 'size', 'width': 300, 'height': 225},
            2: {'stage': 'crop', 'x': 0.0, 'y': 0.0, 'width': 1.0, 'height': 1.0},
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert set(completions.keys()) == {'0', '1', '2'}

    returned_base_64_image = completions['2']['image']
    returned_image, _ = base64_to_numpy(returned_base_64_image)
    assert (content_image == returned_image).all()


def test_unchanged_with_mask_only(client, content_mask):
    base64_mask = numpy_to_base64(content_mask)

    # expect unchanged
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 50,
                'contrast': 50,
                'red': 50,
                'green': 50,
                'blue': 50,
            },
            1: {'stage': 'size', 'width': 300, 'height': 225},
            2: {'stage': 'crop', 'x': 0.0, 'y': 0.0, 'width': 1.0, 'height': 1.0},
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(mask=base64_mask, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert set(completions.keys()) == {'0', '1', '2'}

    returned_base_64_mask = completions['2']['mask']
    returned_mask, _ = base64_to_numpy(returned_base_64_mask, cv2.IMREAD_GRAYSCALE)
    assert (content_mask == returned_mask).all()


def test_unchanged_with_image_and_mask(client, style_image, style_mask):
    base64_image = numpy_to_base64(style_image)
    base64_mask = numpy_to_base64(style_mask)

    # expect unchanged
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 50,
                'contrast': 50,
                'red': 50,
                'green': 50,
                'blue': 50,
            },
            1: {'stage': 'size', 'width': 224, 'height': 224},
            2: {'stage': 'crop', 'x': 0.0, 'y': 0.0, 'width': 1.0, 'height': 1.0},
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, mask=base64_mask, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert set(completions.keys()) == {'0', '1', '2'}

    returned_base_64_image = completions['2']['image']
    returned_base_64_mask = completions['2']['mask']
    returned_image, _ = base64_to_numpy(returned_base_64_image, cv2.IMREAD_COLOR)
    returned_mask, _ = base64_to_numpy(returned_base_64_mask, cv2.IMREAD_GRAYSCALE)

    assert (style_image == returned_image).all()
    assert (style_mask == returned_mask).all()
