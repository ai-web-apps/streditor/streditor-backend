def test_database_initialization(cli_runner):
    result = cli_runner.invoke(args=['init-db'])
    assert 'Initialized the database.' in result.output


def test_streditor_index(client):
    rv = client.get('/')
    assert rv.data == b'Streditor backend is live.'


def test_preprocessing_index(client):
    rv = client.get('/preprocessing/')
    assert rv.data == b'Preprocessing views are live.'


def test_masking_index(client):
    rv = client.get('/masking/')
    assert rv.data == b'Masking views are live.'


def test_stram_index(client):
    rv = client.get('/stram/')
    assert rv.data == b'Stram views are live.'
