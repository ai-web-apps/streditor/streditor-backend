import cv2
import json

from streditor.utils.conversion import base64_to_numpy, numpy_to_base64


def test_adjust_colour(client, content_image):
    base64_image = numpy_to_base64(content_image)

    # expect unchanged
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 50,
                'contrast': 50,
                'red': 50,
                'green': 50,
                'blue': 50,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_image, _ = base64_to_numpy(returned_base_64_image)
    assert (content_image == returned_image).all()

    # increase image contrast
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 50,
                'contrast': 55,
                'red': 50,
                'green': 50,
                'blue': 50,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_image, _ = base64_to_numpy(returned_base_64_image)
    assert content_image.std() < returned_image.std()

    # decrease image brightness
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 45,
                'contrast': 50,
                'red': 50,
                'green': 50,
                'blue': 50,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_image, _ = base64_to_numpy(returned_base_64_image)
    assert content_image.mean() > returned_image.mean()

    # increase red, decrease green, maintain blue
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 50,
                'contrast': 50,
                'red': 78,
                'green': 17,
                'blue': 50,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_image = completions['0']['image']
    returned_image, _ = base64_to_numpy(returned_base_64_image)
    assert (content_image[..., 0] == returned_image[..., 0]).all()
    assert content_image[..., 1].mean() > returned_image[..., 1].mean()
    assert content_image[..., 2].mean() < returned_image[..., 2].mean()


def test_adjust_colour_with_mask(client, content_image, content_mask):
    base64_image = numpy_to_base64(content_image)
    base64_mask = numpy_to_base64(content_mask)

    # expect mask to be completely unchanged
    executions = json.dumps(
        {
            0: {
                'stage': 'colour',
                'brightness': 40,
                'contrast': 31,
                'red': 23,
                'green': 50,
                'blue': 81,
            }
        }
    )
    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, mask=base64_mask, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    returned_base_64_mask = completions['0']['mask']
    returned_mask, _ = base64_to_numpy(returned_base_64_mask, cv2.IMREAD_GRAYSCALE)
    assert (content_mask == returned_mask).all()
