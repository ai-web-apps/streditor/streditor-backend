import json
import pytest
import numpy as np
from stram.utils.json_processing import Config

from streditor.utils.conversion import (
    base64_to_numpy,
    numpy_to_base64,
    convert_numpy_mask,
)


@pytest.fixture(scope='module')
def gatys_config():
    yield Config(
        method='gatys',
        max_iterations=20,
        early_stopping=dict(enabled=False, delta=0.01, patience=20),
        optimizer='Adam',
        optimizer_params=dict(beta_1=0.99, epsilon=1e-1),
        learning_rate_params=dict(
            decay_steps=20, initial_learning_rate=2.0, end_learning_rate=0.2, power=2.0
        ),
        content_layers=dict(block2_conv2=1.0),
        style_layers=dict(block1_conv2=0.5, block2_conv2=0.5),
        content_loss_weight=1.0,
        style_loss_weight=0.1,
        variation_loss_weight=0.1,
    )


@pytest.fixture(scope='module')
def yijun_config():
    yield Config(
        method='yijun',
        style_bottom_layer='block2_conv2',
        style_strength=0.5,
        smoothing_tool='guided_filter',
        smoothing=dict(epsilon=3, window_radius=5),
    )


@pytest.fixture(scope='module')
def reinhard_config():
    yield Config(
        method='reinhard',
        style_strength=0.82,
    )


def test_stram_package_import():
    from stram.domain.methods.gatys_method import GatysMethod
    from stram.domain.methods.yijun_method import YijunMethod
    from stram.domain.methods.reinhard_method import ReinhardMethod

    gatys = GatysMethod()
    yijun = YijunMethod()
    reinhard = ReinhardMethod()

    assert str(gatys) == 'gatys'
    assert str(yijun) == 'yijun'
    assert str(reinhard) == 'reinhard'


def test_get_hash(client, content_image):
    base64_image = numpy_to_base64(content_image)
    executions = json.dumps({0: {'stage': 'input'}})

    rv = client.post(
        '/preprocessing/execute/',
        data=dict(image=base64_image, executions=executions),
    )
    completions = rv.get_json()['completions']
    assert '0' in completions.keys()

    # check hash integrity
    hash = completions['0']['hash']
    assert hash == '43ddda65d7f419a5a14768fa138303823f22a6c585594215a7adcee3436809e7'


def test_order_gatys(client, gatys_config, content_image, style_image):
    rv = _run_test_order(client, gatys_config, content_image, style_image)
    returned_base_64_image = rv.get_json()['image']
    returned_image_hash = rv.get_json()['hash']
    returned_image, _ = base64_to_numpy(returned_base_64_image)

    assert rv.status_code == 200
    assert returned_image.shape == content_image.shape
    assert returned_image.dtype == content_image.dtype
    assert isinstance(returned_image_hash, str)
    assert len(returned_image_hash) == 64


def test_order_yijun(client, yijun_config, content_image, style_image):
    rv = _run_test_order(client, yijun_config, content_image, style_image)
    returned_base_64_image = rv.get_json()['image']
    returned_image_hash = rv.get_json()['hash']
    returned_image, _ = base64_to_numpy(returned_base_64_image)

    assert rv.status_code == 200
    assert returned_image.shape == content_image.shape
    assert returned_image.dtype == content_image.dtype
    assert isinstance(returned_image_hash, str)
    assert len(returned_image_hash) == 64


def test_order_reinhard(client, reinhard_config, content_image, style_image):
    rv = _run_test_order(client, reinhard_config, content_image, style_image)
    returned_base_64_image = rv.get_json()['image']
    returned_image_hash = rv.get_json()['hash']
    returned_image, _ = base64_to_numpy(returned_base_64_image)

    assert rv.status_code == 200
    assert returned_image.shape == content_image.shape
    assert returned_image.dtype == content_image.dtype
    assert isinstance(returned_image_hash, str)
    assert len(returned_image_hash) == 64


def test_order_gatys_with_both_masks(
    client, gatys_config, content_image, style_image, content_mask, style_mask
):
    rv = _run_test_order(
        client, gatys_config, content_image, style_image, content_mask, style_mask
    )
    returned_base_64_image = rv.get_json()['image']
    returned_image_hash = rv.get_json()['hash']
    returned_image, _ = base64_to_numpy(returned_base_64_image)

    assert rv.status_code == 200
    assert returned_image.shape == content_image.shape
    assert returned_image.dtype == content_image.dtype
    assert isinstance(returned_image_hash, str)
    assert len(returned_image_hash) == 64


def test_order_yijun_with_content_mask(
    client, yijun_config, content_image, style_image, content_mask
):
    binary_content_mask = np.copy(content_mask)
    binary_content_mask[content_mask > 0] = 1

    rv = _run_test_order(
        client, yijun_config, content_image, style_image, content_mask=binary_content_mask
    )
    returned_base_64_image = rv.get_json()['image']
    returned_image_hash = rv.get_json()['hash']
    returned_image, _ = base64_to_numpy(returned_base_64_image)

    assert rv.status_code == 200
    assert returned_image.shape == content_image.shape
    assert returned_image.dtype == content_image.dtype
    assert isinstance(returned_image_hash, str)
    assert len(returned_image_hash) == 64


def test_order_reinhard_with_style_mask(
    client, reinhard_config, content_image, style_image, style_mask
):
    binary_style_mask = np.copy(style_mask)
    binary_style_mask[style_mask > 0] = 1

    rv = _run_test_order(
        client, reinhard_config, content_image, style_image, style_mask=binary_style_mask
    )
    returned_base_64_image = rv.get_json()['image']
    returned_image_hash = rv.get_json()['hash']
    returned_image, _ = base64_to_numpy(returned_base_64_image)

    assert rv.status_code == 200
    assert returned_image.shape == content_image.shape
    assert returned_image.dtype == content_image.dtype
    assert isinstance(returned_image_hash, str)
    assert len(returned_image_hash) == 64


def test_invalid_order(client, content_image, style_image):
    styling_config = Config(method='unkown')
    rv = _run_test_order(client, styling_config, content_image, style_image)

    assert rv.status_code == 500
    assert rv.get_json()['error'] == 'Invalid order: Method unkown does not exist'


def test_invalid_order_with_masks(
    client, gatys_config, content_image, style_image, content_mask, style_mask
):
    invalid_content_mask = np.copy(content_mask)
    invalid_content_mask[content_mask == 2] = 3

    with pytest.raises(ValueError):
        _run_test_order(
            client,
            gatys_config,
            content_image,
            style_image,
            invalid_content_mask,
            style_mask,
        )


def _run_test_order(
    client, styling_config, content_image, style_image, content_mask=None, style_mask=None
):
    string_styling_config = json.dumps(styling_config)
    base64_content_image = numpy_to_base64(content_image)
    base64_style_image = numpy_to_base64(style_image)

    base64_content_mask = convert_numpy_mask(content_mask)
    base64_style_mask = convert_numpy_mask(style_mask)

    return client.post(
        '/stram/order/',
        data=dict(
            stylingConfig=string_styling_config,
            baseImage=base64_content_image,
            styleImage=base64_style_image,
            baseMask=base64_content_mask,
            styleMask=base64_style_mask,
        ),
    )


def test_export_figure(client, content_image, style_image):
    base64_content_image = numpy_to_base64(content_image)
    base64_style_image = numpy_to_base64(style_image)

    rv = client.post(
        '/stram/export/',
        data=dict(
            baseImage=base64_content_image,
            styleImage=base64_style_image,
            synthesizedImage=base64_content_image,
        ),
    )
    base64_figure = rv.get_json()['figure']
    numpy_figure, _ = base64_to_numpy(base64_figure)

    assert rv.status_code == 200
    assert numpy_figure.shape[0] > content_image.shape[0]
    assert numpy_figure.shape[1] > 2 * content_image.shape[1]
