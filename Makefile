SHELL:=bash
username?=$(shell whoami)

.PHONY: install
install:
	conda env create -f environment.yml

.PHONY: run
run:
	flask run

.PHONY: test
test:
	pytest tests

.PHONY: coverage
coverage:
	STRAM_AUTOGRAPH=False coverage run --source stram -m pytest tests
	coverage html -i
ifeq ($(shell uname -s), Windows_NT)
	start chrome htmlcov/index.html
else
	google-chrome htmlcov/index.html
endif
