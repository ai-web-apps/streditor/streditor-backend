import numpy as np
from stram.domain.toolsets.general_toolset import validate_masks, get_colour_map


def _sanitize_mask(mask):
    if mask is None:
        return np.asarray([1], dtype=np.uint8)

    non_zero_labels = np.setdiff1d(mask, [0])
    if len(non_zero_labels) != 1:
        return mask

    sanitized_mask = np.copy(mask)
    sanitized_mask[mask == non_zero_labels[0]] = 1
    return sanitized_mask


def masks_are_valid(content_mask, style_mask):
    sanitized_content_mask = _sanitize_mask(content_mask)
    sanitized_style_mask = _sanitize_mask(style_mask)

    try:
        validate_masks(sanitized_content_mask, sanitized_style_mask)
        return True
    except ValueError:
        return False


def compute_colour_mask(mask):
    colour_map = get_colour_map()
    coloured_mask = np.empty(mask.shape + (3,), dtype=np.uint8)

    for label in np.unique(mask):
        if label == 0:
            colour = (np.array([0, 0, 0], dtype=np.uint8),)  # black
        else:
            colour = colour_map[label % len(colour_map)]
        coloured_mask[mask == label] = colour

    return coloured_mask
