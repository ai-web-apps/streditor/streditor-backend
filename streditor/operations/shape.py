import cv2


def crop_image(image, p_x, p_y, p_width, p_height):
    image_width = image.shape[1]
    image_height = image.shape[0]

    left_x = round(p_x * image_width)
    top_y = round(p_y * image_height)
    right_x = left_x + round(p_width * image_width)
    bottom_y = top_y + round(p_height * image_height)

    assert 0 <= left_x < right_x <= image_width
    assert 0 <= top_y < bottom_y <= image_height
    return image[top_y:bottom_y, left_x:right_x]


def crop_mask(mask, p_x, p_y, p_width, p_height):
    return crop_image(mask, p_x, p_y, p_width, p_height)


def resize_image(image, width, height):
    return cv2.resize(image, (width, height), interpolation=cv2.INTER_LINEAR)


def resize_mask(mask, width, height):
    return cv2.resize(mask, (width, height), interpolation=cv2.INTER_NEAREST)
