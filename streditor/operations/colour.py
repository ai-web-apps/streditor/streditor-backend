import numpy as np


def adjust_image_colours(image, gain, bias, red_boost, green_boost, blue_boost):
    adjusted_image = image.astype(np.float32) + [blue_boost, green_boost, red_boost]
    adjusted_image = gain * adjusted_image + bias
    adjusted_image = np.clip(adjusted_image, 0, 255)

    return np.round(adjusted_image).astype(np.uint8)
