from stram.utils.hashing import get_image_hash
from stram.utils.export import create_styling_process_figure
from stram.services.method_factory import create_method


def get_hash(image):
    return get_image_hash(image)


def fulfil_order(styling_config, content_image, style_image, content_mask, style_mask):
    method = create_method(styling_config.method)
    method.set_up(styling_config, content_image, style_image, content_mask, style_mask)
    method.process(styling_config)

    synthesized_image = method.get_synthesized_image()
    return synthesized_image, get_image_hash(synthesized_image)


def create_figure(content_image, style_image, synthesized_image):
    return create_styling_process_figure(content_image, style_image, synthesized_image)
