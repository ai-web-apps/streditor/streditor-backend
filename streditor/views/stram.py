import json
from flask import request, Response, Blueprint
from stram.utils.exceptions import InvalidOrderException

from streditor.executions.stram import stram_executor, figure_creator

bp = Blueprint('stram', __name__, url_prefix='/stram')


@bp.route('/', methods=('GET',))
def index():
    return 'Stram views are live.'


@bp.route('/order/', methods=('POST',))
def order():
    styling_dict = json.loads(request.form['stylingConfig'])
    base64_content_image = request.form['baseImage']
    base64_style_image = request.form['styleImage']
    base64_content_mask = request.form.get('baseMask', None)
    base64_style_mask = request.form.get('styleMask', None)

    try:
        base64_synthesized_image, image_hash = stram_executor(
            styling_dict,
            base64_content_image,
            base64_style_image,
            base64_content_mask,
            base64_style_mask,
        )
        response = dict(image=base64_synthesized_image, hash=image_hash)
        return Response(json.dumps(response), status=200, content_type='application/json')

    except InvalidOrderException as ioe:
        response = dict(error=str(ioe))
        return Response(json.dumps(response), status=500, content_type='application/json')


@bp.route('/export/', methods=('POST',))
def export_figure():
    base64_content_image = request.form['baseImage']
    base64_style_image = request.form['styleImage']
    base64_synthesized_image = request.form['synthesizedImage']

    try:
        base64_figure = figure_creator(
            base64_content_image, base64_style_image, base64_synthesized_image
        )
        response = dict(figure=base64_figure)
        return Response(json.dumps(response), status=200, content_type='application/json')

    except InvalidOrderException as ioe:
        response = dict(error=str(ioe))
        return Response(json.dumps(response), status=500, content_type='application/json')
