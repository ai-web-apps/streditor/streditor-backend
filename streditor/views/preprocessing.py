import json
from flask import request, Response, Blueprint

from streditor.executions.preprocessing import preprocessing_executor

bp = Blueprint('preprocessing', __name__, url_prefix='/preprocessing')


@bp.route('/', methods=('GET',))
def index():
    return 'Preprocessing views are live.'


@bp.route('/execute/', methods=('POST',))
def preprocess():
    base64_image = request.form.get('image', None)
    base64_mask = request.form.get('mask', None)

    executions = json.loads(request.form['executions'])
    executions = {int(k): v for k, v in executions.items()}

    completions = preprocessing_executor(base64_image, base64_mask, executions)
    response = dict(completions=completions)

    return Response(json.dumps(response), status=200, content_type='application/json')
