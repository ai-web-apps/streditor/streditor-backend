import json
from flask import request, Response, Blueprint

from streditor.executions.masking import perform_masks_validation, perform_mask_colouring

bp = Blueprint('masking', __name__, url_prefix='/masking')


@bp.route('/', methods=('GET',))
def index():
    return 'Masking views are live.'


@bp.route('/colour/', methods=('POST',))
def colour_mask():
    base64_mask = request.form['mask']
    response = perform_mask_colouring(base64_mask)

    return Response(json.dumps(response), status=200, content_type='application/json')


@bp.route('/validate/', methods=('POST',))
def validate_masks_pair():
    base64_content_mask = request.form.get('baseMask', None)
    base64_style_mask = request.form.get('styleMask', None)
    response = perform_masks_validation(base64_content_mask, base64_style_mask)

    return Response(json.dumps(response), status=200, content_type='application/json')
