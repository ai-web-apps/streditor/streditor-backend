import os
from flask import Flask
from flask_cors import CORS
from decouple import config as env_config

if env_config('STRAM_LOGGING', default=False, cast=bool):
    import logging
    import logging.config

    logging.config.fileConfig('logging.conf')

from streditor import db
from streditor.views.stram import bp as stram_bp
from streditor.views.masking import bp as masking_bp
from streditor.views.preprocessing import bp as preprocessing_bp


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.urandom(16), DATABASE=os.path.join(app.instance_path, 'app.sqlite')
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        cors_origins = os.environ['FLASK_CORS_ORIGINS'].split(' ')
        cors_config = dict(origins=cors_origins)
        CORS(app, resources={r'/*': cors_config})
    except KeyError:
        pass

    os.makedirs(app.instance_path, exist_ok=True)

    @app.route('/')
    def index():
        return 'Streditor backend is live.'

    db.init_app(app)
    app.register_blueprint(stram_bp)
    app.register_blueprint(masking_bp)
    app.register_blueprint(preprocessing_bp)

    return app
