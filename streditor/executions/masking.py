import cv2

from streditor.operations.mask import masks_are_valid, compute_colour_mask
from streditor.utils.conversion import (
    base64_to_numpy,
    numpy_to_base64,
    convert_base64_mask,
)


def perform_mask_colouring(base64_mask):
    numpy_mask, mask_header = base64_to_numpy(base64_mask, cv2.IMREAD_GRAYSCALE)
    numpy_colour_mask = compute_colour_mask(numpy_mask)
    base64_colour_mask = numpy_to_base64(numpy_colour_mask, mask_header)

    return dict(colour_mask=base64_colour_mask)


def perform_masks_validation(base64_content_mask, base64_style_mask):
    numpy_content_mask = convert_base64_mask(base64_content_mask)
    numpy_style_mask = convert_base64_mask(base64_style_mask)
    valid = masks_are_valid(numpy_content_mask, numpy_style_mask)

    return dict(valid=valid)
