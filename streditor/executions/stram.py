from stram.utils.json_processing import dict2namespace

from streditor.operations.stram import fulfil_order, create_figure
from streditor.utils.conversion import (
    base64_to_numpy,
    numpy_to_base64,
    convert_base64_mask,
)


def stram_executor(
    styling_dict,
    base64_content_image,
    base64_style_image,
    base64_content_mask,
    base64_style_mask,
):
    styling_config = dict2namespace(styling_dict)
    numpy_content_image, base64_header = base64_to_numpy(base64_content_image)
    numpy_style_image, _ = base64_to_numpy(base64_style_image)

    numpy_content_mask = convert_base64_mask(base64_content_mask)
    numpy_style_mask = convert_base64_mask(base64_style_mask)

    synthesized_numpy_image, image_hash = fulfil_order(
        styling_config,
        numpy_content_image,
        numpy_style_image,
        numpy_content_mask,
        numpy_style_mask,
    )
    return numpy_to_base64(synthesized_numpy_image, base64_header), image_hash


def figure_creator(base64_content_image, base64_style_image, base64_synthesized_image):
    numpy_content_image, base64_header = base64_to_numpy(base64_content_image)
    numpy_style_image, _ = base64_to_numpy(base64_style_image)
    numpy_synthesized_image, _ = base64_to_numpy(base64_synthesized_image)

    figure = create_figure(
        numpy_content_image, numpy_style_image, numpy_synthesized_image
    )
    return numpy_to_base64(figure, base64_header)
