import cv2

from streditor.operations.stram import get_hash
from streditor.operations.colour import adjust_image_colours
from streditor.utils.conversion import base64_to_numpy, numpy_to_base64
from streditor.operations.shape import crop_image, crop_mask, resize_image, resize_mask


def preprocessing_executor(base64_image, base64_mask, executions):
    completions = {}
    if base64_image is not None:
        numpy_image, base64_header = base64_to_numpy(base64_image, cv2.IMREAD_COLOR)
    if base64_mask is not None:
        numpy_mask, mask_header = base64_to_numpy(base64_mask, cv2.IMREAD_GRAYSCALE)

    for exec_index in sorted(executions.keys()):
        execution = executions[exec_index]

        if execution['stage'] == 'input':
            if base64_image is not None:
                execution['hash'] = get_hash(numpy_image)
        else:
            if base64_image is not None:
                numpy_image = perform_image_operation(numpy_image, execution)
                execution['image'] = numpy_to_base64(numpy_image, base64_header)

            if base64_mask is not None:
                numpy_mask = perform_mask_operation(numpy_mask, execution)
                execution['mask'] = numpy_to_base64(numpy_mask, mask_header)

        completions[exec_index] = execution

    return completions


def perform_image_operation(image, execution):
    if execution['stage'] == 'size':
        width = execution['width']
        height = execution['height']

        image = resize_image(image, width, height)

    elif execution['stage'] == 'crop':
        p_x = execution['x']
        p_y = execution['y']
        p_width = execution['width']
        p_height = execution['height']

        image = crop_image(image, p_x, p_y, p_width, p_height)

    elif execution['stage'] == 'colour':
        brightness = execution['brightness']
        contrast = execution['contrast']
        red = execution['red']
        green = execution['green']
        blue = execution['blue']

        gain = 3 ** (contrast / 50 - 1)
        bias = 2 * brightness - 100
        red_boost = 2 * red - 100
        green_boost = 2 * green - 100
        blue_boost = 2 * blue - 100

        image = adjust_image_colours(
            image, gain, bias, red_boost, green_boost, blue_boost
        )

    else:
        raise ValueError(f'Unexpected execution: stage {execution["stage"]}')

    return image


def perform_mask_operation(mask, execution):
    if execution['stage'] == 'size':
        width = execution['width']
        height = execution['height']

        mask = resize_mask(mask, width, height)

    elif execution['stage'] == 'crop':
        p_x = execution['x']
        p_y = execution['y']
        p_width = execution['width']
        p_height = execution['height']

        mask = crop_mask(mask, p_x, p_y, p_width, p_height)

    elif execution['stage'] == 'colour':
        pass

    else:
        raise ValueError(f'Unexpected execution: stage {execution["stage"]}')

    return mask
