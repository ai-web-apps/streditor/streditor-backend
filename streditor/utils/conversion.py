import cv2
import base64
import numpy as np


def base64_to_numpy(base64_image, cv2_channels_flag=cv2.IMREAD_COLOR):
    header, content = base64_image.split(',')
    image_bytes = base64.b64decode(content.encode('utf-8'))
    buffer_image = np.frombuffer(image_bytes, dtype=np.uint8)
    return cv2.imdecode(buffer_image, flags=cv2_channels_flag), header


def numpy_to_base64(numpy_image, header='data:image/png;base64'):
    image_format = f'.{header[11:-7]}'
    _, encoded_image = cv2.imencode(image_format, numpy_image)
    encoded_bytes = base64.b64encode(encoded_image.tobytes()).decode('utf-8')
    return f'{header},{encoded_bytes}'


def convert_base64_mask(base64_mask):
    if base64_mask is None:
        return None
    return base64_to_numpy(base64_mask, cv2_channels_flag=cv2.IMREAD_GRAYSCALE)[0]


def convert_numpy_mask(numpy_mask):
    if numpy_mask is None:
        return None
    return numpy_to_base64(numpy_mask)
